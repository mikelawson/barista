"use strict";

const rules = Symbol("rules");
const val = Symbol("val");
const rulesToUse = Symbol("rulesToUse");
const messageBag = Symbol("messageBag");
const setMessage = Symbol("setMessage");
const responseTextRequired = Symbol("responseTextRequired");
const responseTextEmail = Symbol("responseTextEmail");
const currentMessages = Symbol("currentMessages");

export default class Barista {

    constructor() {
        this[responseTextEmail] = "Invalid Email";
        this[responseTextRequired] = "Field is required";
        this[val] = null;
        this[rulesToUse] = [];
        this[currentMessages] = [];
        this[messageBag] = {

                count: 0,
                getMessages:() => {
                    return this[currentMessages];
                },
                setMessage: (field, message) => {
                    this[currentMessages].push("The field " + field + " is invalid: " + message);
                },
                reset: () => {
                    this[currentMessages] = [];
                    this.count = 0;
                }


        };
        this[rules] = {
            email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            required: (v) => { return v != undefined && v != null && v != ""; }
        };
    }

    useRules(theRules) {
        this[rulesToUse] = theRules;
        if (!this.internalValidation()) {
            this[rulesToUse] = [];
            return false;
        }
    }

    /**
     *
     * @returns {*}
     */
    messages() {
            return this[messageBag];
    }

    reset() {
        this[messageBag].reset();
    }

    /**
     *
     * @param fieldName
     * @param value
     * @returns {boolean}
     */
    validate(fieldName, value) {
        if (this.internalValidation()) {
            let currentFieldIsValid = true;
            for (const rule in this[rulesToUse]) {
                if (this[rulesToUse][rule] == "email" && !value.match(this[rules].email)) {
                    currentFieldIsValid = false;
                    this[messageBag].count += 1;
                    this[messageBag].setMessage(fieldName, this[responseTextEmail]);
                }

                if (this[rulesToUse][rule] == "required" && !this[rules].required(value)) {
                    currentFieldIsValid = false;
                    this[messageBag].count += 1;
                    this[messageBag].setMessage(fieldName, this[responseTextRequired]);
                }
            }
            return currentFieldIsValid;
        }
    }

    isValid() {
        return this[messageBag].count == 0;
    }

    internalValidation() {
        if (this[messageBag] instanceof Object && (this[rulesToUse] instanceof Array || this[rulesToUse] == null)) {

            for (const k in this[rulesToUse]) {

                if (this[rules].hasOwnProperty(this[rulesToUse][k]) == undefined) {
                    this[messageBag].count += 1;
                    this[messageBag].setMessage("", "Validator ruleset does not exist.");
                    return false;
                }
            }
            return true;
        }
        else {
            this[messageBag].count += 1;
            this[messageBag].setMessage("", "Validator settings are invalid.");
            return false;
        }
    }

}


