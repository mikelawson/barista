# Barista, by The Commission Cafe LLC.

## What is Barista?

Barista is a validation module that can be used inside any JavaScript project to quickly

and easily define validation rules for front end tasks. Several rules are built in and you can

even add your own to fit your specific needs. 

## Inspiration

Well, we needed an easy to use and reusable way to validate front end fields for our

flagship product, and the project is growing from there. Future enhancements include:

+ npm modules to validate uncompiled resources, like vue and jsx files

+ defining your own validation rules

+ extending existing ones

+ framework interop

## Author

[Mike Lawson](mike@thecommissioncafe.com)